#!/bin/bash

if [[ -d build ]]; then
    rm -rf build
fi
mkdir build
cd build

ARCH=`arch`
if [ $ARCH = "arm64" ] || [ $ARCH = "aarch64" ]; then
  export QT_HOST_PATH=$PREFIX
fi

cmake .. \
        -G 'Ninja' \
        -DCMAKE_PREFIX_PATH=$PREFIX \
        -DCMAKE_BUILD_TYPE=Release \
        -DDTKSCRIPT_ENABLE_WIDGETS=OFF \
        -DCMAKE_INSTALL_PREFIX="${PREFIX}" -DCMAKE_INSTALL_LIBDIR=lib

cmake --build . --target install
