#pragma once

#include <QtTest>

#define DTKSCRIPTTEST_MAIN_NOGUI(TestMain, TestObject)	\
    int TestMain(int argc, char *argv[]) {              \
        QCoreApplication app(argc, argv);               \
        app.setOrganizationName("inria");               \
        app.setApplicationName("dtkScriptTest");        \
        TestObject tc;                                  \
        return QTest::qExec(&tc, argc, argv);           \
    }
