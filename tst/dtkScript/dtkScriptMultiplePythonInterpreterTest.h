// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#pragma once

#include <QtCore>

class dtkScriptMultiplePythonInterpreterTestCase : public QObject
{
    Q_OBJECT

public:
     dtkScriptMultiplePythonInterpreterTestCase(void);
    ~dtkScriptMultiplePythonInterpreterTestCase(void);

private slots:
    void initTestCase(void);
    void init(void);

private slots:
    void test1Interpreter4Threads(void);
    void test2Interpreters(void);

private slots:
    void cleanupTestCase(void);
    void cleanup(void);

private:
    class PrivateClass *d;
};


//
// dtkCoreIOCompressorTest.h ends here
