// Version: $Id$
//
//

// Commentary:
//
//

// Change Log:
//
//

// Code:

#include <Python.h>

#include "dtkScriptMultiplePythonInterpreterTest.h"

#include <dtkScriptInterpreterPython>

#include <QtCore>


#include <dtkScriptTest>

class PrivateClass {
public:
    QList<QThread> threads;
    void *main_python_threadState = nullptr;
    void *py_interpreter_state_2 = nullptr;
};

// ///////////////////////////////////////////////////////////////////
// dtkCoreIOCompressorTestCase implementation
// ///////////////////////////////////////////////////////////////////
dtkScriptMultiplePythonInterpreterTestCase::dtkScriptMultiplePythonInterpreterTestCase(void): d(new PrivateClass)
{
}

dtkScriptMultiplePythonInterpreterTestCase::~dtkScriptMultiplePythonInterpreterTestCase(void)
{
    delete d;
}

void dtkScriptMultiplePythonInterpreterTestCase::initTestCase(void)
{
    d->main_python_threadState = dtkScriptInterpreterPython::instance()->allowThreads();
    d->py_interpreter_state_2 = dtkScriptInterpreterPython::instance()->newInterpreter();
}

void dtkScriptMultiplePythonInterpreterTestCase::init(void)
{
}

class TheWorker : public QRunnable
{
public:
    void *python_interpreterState = nullptr;

    TheWorker(void *interpreter_state = nullptr) {
        this->python_interpreterState = interpreter_state;
    }

    void run(void) override {
        double current_thread = long(QThread::currentThreadId()) % 100;

        //acquire lock
        qDebug() << "Hello from thread" << current_thread;
        dtkScriptInterpreterPython::instance()->childAcquireLock(python_interpreterState);

        qDebug() << "I have lock" << current_thread;

        //do python stuff
        PyObject* math_string = PyUnicode_FromString((char*)"math");
        PyObject* math_module = PyImport_Import(math_string);
        PyObject* pow_func = PyObject_GetAttrString(math_module,(char*)"pow");

        PyObject* args = PyTuple_Pack(2,
                                      PyFloat_FromDouble(current_thread),
                                      PyFloat_FromDouble(3.0));

        PyObject* py_res = PyObject_CallObject(pow_func, args);
        double res = PyFloat_AsDouble(py_res);

        //release lock
        dtkScriptInterpreterPython::instance()->childReleaseLock();
        qDebug() << "lock released"  << current_thread;

        //test result
        double true_res = current_thread * current_thread * current_thread;
        QCOMPARE(res, true_res);
    }

};

void dtkScriptMultiplePythonInterpreterTestCase::test1Interpreter4Threads(void)
{
    QThreadPool threads;
    threads.setMaxThreadCount(4);

    QList< TheWorker *> workers;
    for(int i=0; i < 4; ++i) {
        TheWorker *w = new TheWorker();
        workers.push_back(w);
        threads.start(w);
    }
}

void dtkScriptMultiplePythonInterpreterTestCase::test2Interpreters(void)
{
    QThreadPool threads;
    threads.setMaxThreadCount(4);

    QList< TheWorker *> workers_interpreter1;
    QList< TheWorker *> workers_interpreter2;

    Q_ASSERT(d->py_interpreter_state_2);

    for(int i=0; i < 2; ++i) {
        TheWorker *w = new TheWorker();
        TheWorker *w_2 = new TheWorker(d->py_interpreter_state_2);

        workers_interpreter1.push_back(w);
        workers_interpreter2.push_back(w_2);

        threads.start(w);
        threads.start(w_2);
    }
}

void dtkScriptMultiplePythonInterpreterTestCase::cleanup(void)
{
}

void dtkScriptMultiplePythonInterpreterTestCase::cleanupTestCase(void)
{
    dtkScriptInterpreterPython::instance()->deleteInterpreter(d->py_interpreter_state_2);
    dtkScriptInterpreterPython::instance()->endAllowThreads();
    dtkScriptInterpreterPython::instance()->release();
}

DTKSCRIPTTEST_MAIN_NOGUI(dtkScriptMultiplePythonInterpreterTest, dtkScriptMultiplePythonInterpreterTestCase)

//
// dtkScriptMultiplePythonInterpreterTest.cpp ends here
