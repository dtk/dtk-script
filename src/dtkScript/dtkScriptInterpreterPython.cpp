// dtkScriptInterpreterPython.cpp ---
//

#include <Python.h>

#include "dtkScriptInterpreterPython.h"

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

class dtkScriptInterpreterPythonPrivate
{
public:
    QString buffer;
    PyThreadState *main_thread_state = nullptr;

    QHash<PyInterpreterState *, QMutex *> interpreter_mutex;
    QHash<PyInterpreterState *, PyThreadState *> thread_state_map;
};

// /////////////////////////////////////////////////////////////////
//
// /////////////////////////////////////////////////////////////////

dtkScriptInterpreterPython *dtkScriptInterpreterPython::instance(void)
{
    if(!s_instance) {
        s_instance = new dtkScriptInterpreterPython;
    }
    return s_instance;
}

dtkScriptInterpreterPython *dtkScriptInterpreterPython::s_instance = nullptr;

dtkScriptInterpreterPython::dtkScriptInterpreterPython(void) : dtkScriptInterpreter(), d(new dtkScriptInterpreterPythonPrivate)
{

    if (qEnvironmentVariableIsSet("CONDA_PREFIX")) {
    // needed wen running inside a conda environment
#if defined(Q_OS_MACOS)
    QString command = "python -c \"import sys; print(sys.prefix)\"";

    QProcess process;
    process.start(command);
    process.waitForFinished(-1); // will wait forever until finished
    QString stdout_str = process.readAllStandardOutput();
    QString stderr_str = process.readAllStandardError();
    qputenv("PYTHONHOME", stdout_str.trimmed().toUtf8());
#endif

#if defined(Q_OS_WIN)
    QString command = "python -c \"import sys; print(';'.join([path for path in sys.path if path]))\"";

    QProcess process;
    process.start(command);
    process.waitForFinished(-1); // will wait forever until finished
    QString stdout_str = process.readAllStandardOutput();
    QString stderr_str = process.readAllStandardError();
    qputenv("PYTHONPATH", stdout_str.trimmed().toUtf8());
#endif
    }

    Py_InitializeEx(0);
    QSettings::setDefaultFormat(QSettings::IniFormat);
}

dtkScriptInterpreterPython::~dtkScriptInterpreterPython(void)
{
    delete d;
    d = nullptr;
}

void dtkScriptInterpreterPython::init(const QString& settings_file)
{
    QString paths;

    if(settings_file.isEmpty()) {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-script");
        paths = settings.value("python-modules/path").toString();
    } else {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", settings_file);
        paths = settings.value("python-modules/path").toString();
    }

    PyRun_SimpleString("import sys");

    auto path_list = paths.split(":", Qt::SkipEmptyParts);
    for (QString path : path_list) {
        PyRun_SimpleString(qPrintable(QString("sys.path.append('%1')").arg(path)));
    }
}

void *dtkScriptInterpreterPython::allowThreads(void)
{
    //init python threads
    d->main_thread_state = PyEval_SaveThread();
    QMutex *m = new QMutex;
    d->interpreter_mutex.insert(d->main_thread_state->interp, m);
    return static_cast<void *>(d->main_thread_state);
}
void dtkScriptInterpreterPython::endAllowThreads(void)
{
    if(d->main_thread_state)
        PyEval_RestoreThread(d->main_thread_state);
    delete d->interpreter_mutex.take(d->main_thread_state->interp);
    d->main_thread_state = nullptr;
}

void dtkScriptInterpreterPython::childAcquireLock(void *interpreter_state)
{
    PyThreadState *pThreadState = nullptr;
    if(interpreter_state) {
        // a interpreter_state is given so we create a thread_state to associate this thread with this interpreter
        PyInterpreterState *interp = static_cast<PyInterpreterState *>(interpreter_state);
        d->interpreter_mutex[interp]->lock();
        pThreadState = PyThreadState_New(interp);
    } else {
        // if no interpreterState is given, use main python interpreter, so main thread_state
        Q_ASSERT(d->main_thread_state);
        d->interpreter_mutex[d->main_thread_state->interp]->lock();
        pThreadState = d->main_thread_state;
    }

    Q_ASSERT(pThreadState);

    PyEval_RestoreThread(pThreadState);
}

void dtkScriptInterpreterPython::childReleaseLock(void)
{
    //check if the thread locked is the main one or not
    if(PyThreadState_Get() != d->main_thread_state) {
        PyThreadState *pThreadState= PyThreadState_Get();
        d->interpreter_mutex[pThreadState->interp]->unlock();
        PyThreadState_Clear(pThreadState);
        PyThreadState_DeleteCurrent();
    } else {
        PyEval_ReleaseThread(d->main_thread_state);
        d->interpreter_mutex[d->main_thread_state->interp]->unlock();
    }
}

void *dtkScriptInterpreterPython::newInterpreter(void)
{
    PyEval_AcquireThread(d->main_thread_state) ; // nb: get the GIL
    PyThreadState* pThreadState = Py_NewInterpreter() ;
    assert( pThreadState != NULL ) ;
    d->thread_state_map.insert(pThreadState->interp, pThreadState);
    PyThreadState_Swap(d->main_thread_state);
    PyEval_ReleaseThread(d->main_thread_state);
    d->interpreter_mutex.insert(pThreadState->interp, new QMutex);
    return static_cast<void *>(pThreadState->interp);
}

void dtkScriptInterpreterPython::deleteInterpreter(void *interpreter_state)
{
    Q_ASSERT(interpreter_state);

    // use PyInterpreterState_GETID() ?
    if(d->main_thread_state->interp == interpreter_state) {
        qWarning() << Q_FUNC_INFO << "use endAllowThread to delete the main interpreter";
        return;
    }

    PyInterpreterState *interp = static_cast<PyInterpreterState *>(interpreter_state);
    auto mutex = d->interpreter_mutex.take(interp);
    mutex->lock();
    auto thread_state = d->thread_state_map.take(interp);
    PyEval_RestoreThread(thread_state);
    PyInterpreterState_Clear(interp);
    PyEval_ReleaseThread(thread_state);
    PyInterpreterState_Delete(interp);
    mutex->unlock();
    delete mutex;
}

void dtkScriptInterpreterPython::initScript(void)
{
    QSettings settings(QSettings::IniFormat, QSettings::UserScope, "inria", "dtk-script");
    settings.beginGroup("init");
    QString init = settings.value("script").toString();
    settings.endGroup();

    if (!init.isEmpty()) {
        PyRun_SimpleString(QString("execfile('%1')").arg(init).toUtf8().constData());
    } else {
        qWarning() << Q_FUNC_INFO << "No init function";
    }
}

void dtkScriptInterpreterPython::release(void)
{
    Q_ASSERT(Py_FinalizeEx() == 0);
}

QString dtkScriptInterpreterPython::interpret(const QString &command, int *stat, void *interpreter_state) {
    bool threadsAllowed = d->main_thread_state ? true: false;

    if(threadsAllowed) {
        childAcquireLock(interpreter_state);
    }
    
    QString statement = command;

    if (command.endsWith(":")) {
        if(!d->buffer.isEmpty())
            d->buffer.append("\n");

        d->buffer.append(command);
        return QString();
    }

    if (!command.isEmpty() && command.startsWith(" ")) {
        if(!d->buffer.isEmpty())
            d->buffer.append("\n");

        d->buffer.append(command);
        return QString();
    }

    if (command.isEmpty() && !d->buffer.isEmpty()) {
        if(!d->buffer.isEmpty())
            d->buffer.append("\n");

        statement = d->buffer;
        d->buffer.clear();
    }

    if (statement.isEmpty())
        return QString();

    PyObject *module = PyImport_AddModule("__main__");
    switch (PyRun_SimpleString(qPrintable(statement))) {
        case  0: *stat = Status_Ok;    break;
        case -1: *stat = Status_Error; break;
        default: break;
    }

    PyErr_Print();

    if(threadsAllowed) {
        childReleaseLock();
    }

    return QString();
}

//
// dtkScriptInterpreterPython.cpp ends here
