// dtkScriptInterpreter.h ---
//

#pragma once

#include <dtkScriptExport>

#include <QtCore>

// /////////////////////////////////////////////////////////////////
// dtkScriptInterpreter
// /////////////////////////////////////////////////////////////////

class DTKSCRIPT_EXPORT dtkScriptInterpreter : public QObject
{
    Q_OBJECT

public:
    enum Status {
        Status_Ok,
        Status_Error,
        Status_Return,
        Status_Break,
        Status_Continue
    };

public:
     dtkScriptInterpreter(QObject *parent = 0);
    ~dtkScriptInterpreter(void);

public slots:
    virtual QString interpret(const QString& command, int *stat, void *interpreter_state = nullptr) = 0;
    virtual void init(const QString& settings_file = "");

private:
    class dtkScriptInterpreterPrivate *d;
};

//
// dtkScriptInterpreter.h ends here
