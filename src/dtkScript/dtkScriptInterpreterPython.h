// dtkScriptInterpreterPython.h ---
//

#pragma once

#include <dtkScriptExport>

#include "dtkScriptInterpreter.h"

class DTKSCRIPT_EXPORT dtkScriptInterpreterPython : public dtkScriptInterpreter
{
    Q_OBJECT

public:
     static dtkScriptInterpreterPython *instance();

protected:
     static dtkScriptInterpreterPython *s_instance;

public slots:
    QString interpret(const QString& command, int *stat, void *interpreter_state = nullptr) override;
    void init(const QString& settings_file = "") override;

    //  need to be called from main interpreter
    void *allowThreads(void);
    void endAllowThreads(void);
    void *newInterpreter(void);
    void deleteInterpreter(void *interpreter_state);

    // need to be called from child thread
    //DO NOT call these 2 functions before interpret, it is done behind the scene
    void childAcquireLock(void *interpreter_state = nullptr);
    void childReleaseLock(void);

    void initScript(void);
    void release(void);

private:
      dtkScriptInterpreterPython(void);
     ~dtkScriptInterpreterPython(void);

private:
    class dtkScriptInterpreterPythonPrivate *d = nullptr;
};

//
// dtkScriptInterpreterPython.h ends here
