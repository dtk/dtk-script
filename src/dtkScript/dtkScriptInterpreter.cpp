// dtkScriptInterpreter.cpp ---
//

#include "dtkScriptInterpreter.h"

// /////////////////////////////////////////////////////////////////
// dtkScriptInterpreterPrivate
// /////////////////////////////////////////////////////////////////

class dtkScriptInterpreterPrivate // Not used so not initialized in the class
{
public:
};

// /////////////////////////////////////////////////////////////////
// dtkScriptInterpreter
// /////////////////////////////////////////////////////////////////

dtkScriptInterpreter::dtkScriptInterpreter(QObject *parent) : QObject(parent)
{

}

dtkScriptInterpreter::~dtkScriptInterpreter(void)
{

}

void dtkScriptInterpreter::init(const QString& /*settings_file = ""*/)
{

}

//
// dtkScriptInterpreter.cpp ends here
