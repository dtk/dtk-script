// dtkScriptInterpreterWidget.h ---
//

#pragma once

#include <dtkScriptWidgetsExport>

#include <QtWidgets>

class dtkScriptInterpreter;

class DTKSCRIPTWIDGETS_EXPORT dtkScriptInterpreterWidget : public QPlainTextEdit
{
    Q_OBJECT

public:
     dtkScriptInterpreterWidget(QWidget *parent = 0);
    ~dtkScriptInterpreterWidget(void);

#pragma mark -
#pragma mark - Interpretation interface

    dtkScriptInterpreter *interpreter(void);

    void registerInterpreter(dtkScriptInterpreter *interpreter);

#pragma mark -
#pragma mark - Graphical interface

    void output(const QString& result);

protected:
    void keyPressEvent(QKeyEvent *event) override;
    void mousePressEvent(QMouseEvent *event) override;
    void mouseReleaseEvent(QMouseEvent *event) override;

protected:
    void onKeyUpPressed(void);
    void onKeyDownPressed(void);
    void onKeyLeftPressed(void);
    void onKeyRightPressed(void);
    void onKeyEnterPressed(void);

protected:
    int currentLineNumber(void) const;
    QString currentLine(void) const;
    QString currentBlock(void) const;

private:
    class dtkScriptInterpreterWidgetPrivate *d = nullptr;
};

//
// dtkScriptInterpreterWidget.h ends here
