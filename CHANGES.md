# Changelog

## version 3.2.0 - 2025-03-06
- remove swig interface files and move them to dtk-core-python
- remove path to swig interface files in the target dtk::Script 

## version 3.1.1 - 2025-01-09
- fix recipe

## version 3.1.0 - 2025-01-09
- switch to qt 6.8
- use latest compilers
- new version of numpy.i to fit with latest swig versions
- remove DTK_SWIG_INCLUDE_PATH variable and replace it by a standard way to export path using cmake targets
- new cmake targets:
  - dtk::Script that contains path to files dtkBase.i, numpy.i, QVariant.i and qrunnable.i. Using target_link_library(MYTARGET PRIVATE dtk::Script) enables to retrieve this path.
  - dtk::ScriptWidgets

## version 3.0.0 - 2023-09-25
- update to Qt6
- build for python 3.9, 3.10, 3.11
- auto-lock if allowThreads
- lock handling around interpret

## version 2.5.2 - 2022-01-06
 - better wrapping of QVariant
 - dont specify visual studio version

## version 2.5.1 - 2021-10-21
 - removed the installation of handlers by the python console when embedded.

## version 2.5.0 - 2021-08-05
 - add wrapping for QList<QString> (needed for dtkCoreParameters)
 - fix deleteInterpreter for python 3.9
 - fix wrapping for const qlonglong&

## version 2.4.0 - 2020-04-23
 - qvariantMap wrapping
 - qvariant wrapping
 - bugfix qlonglong wrapping

## version 2.3.1 - 2019-09-20
- fix swig wrapping
- fix for macos when running in a conda environment
- windows: fix export for dtkScriptWidget

## version 2.3.0 - 2019-09-19
- add swig wrapping
- add cmake option to compile widgets

## version 2.2.1 - 2019-07-08
- fix cmake for windows

## version 2.2.0 - 2019-01-10
- methods to allow threads in python

## version 2.1.1 - 2018-11-29
- hotfix on CXX_FLAGS

## version 2.1.0 - 2018-11-29
- first release
